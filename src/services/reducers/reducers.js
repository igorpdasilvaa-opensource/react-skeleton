import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { reducer as toastrReducer } from 'react-redux-toastr';
import auth from './auth';

const rootReducer = combineReducers({
  auth,
  form: formReducer,
  toastr: toastrReducer,
});

export default rootReducer;
