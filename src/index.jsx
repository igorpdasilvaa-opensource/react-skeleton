/* eslint-disable no-underscore-dangle */
import './config/jquery';
import './config/imports';
import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter } from 'react-router-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import promise from 'redux-promise';
import thunk from 'redux-thunk';
import multi from 'redux-multi';
import Routes from './config/routes';

import Header from './components/stateless/template/header';
import Footer from './components/stateless/template/footer';
import Sidebar from './components/stateless/template/sidebar';
import reducers from './services/reducers/reducers';
import Messages from './utils/msg/messages';
import * as serviceWorker from './config/serviceWorker';

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
const store = applyMiddleware(multi, thunk, promise)(createStore)(reducers, devTools);

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <HashRouter>
        <div className="wrapper">
          <Header />
          <Sidebar />
          <Routes />
          <Footer />
          <Messages />
        </div>
      </HashRouter>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
);

serviceWorker.unregister();
