import React from 'react';
import { Switch, Route, Redirect } from 'react-router';

import Hello from '../pages/hello';
import Bye from '../pages/bye';
import Login from '../pages/login';

export default () => (
  <div className="content-wrapper">
    <Switch>
      <Route exact path="/" component={Hello} />
      <Route path="/bye" component={Bye} />
      <Route path="/login" component={Login} />
      <Redirect from="*" to="/" />
    </Switch>
  </div>
);
