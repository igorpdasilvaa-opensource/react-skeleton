import React from 'react';
import { Field } from 'redux-form';
import LabelAndInput from '../../utils/form/labelAndInput';

export default () => ([
  <Field
    name="email"
    component={LabelAndInput}
    label="Email"
    cols="12 6"
    placeholder="informe o email"
  />,
  <Field
    name="passwd"
    component={LabelAndInput}
    label="Senha"
    cols="12 6"
    placeholder="informe a senha"
    type="password"
  />,
]

);
