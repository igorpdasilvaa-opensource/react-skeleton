import React from 'react';

export default () => (
  <nav className="main-header navbar navbar-collapsed navbar-white navbar-light">

    <ul className="navbar-nav">
      <li className="nav-item">
        <a aria-label="toggle-sidebar" className="nav-link" data-widget="pushmenu" href role="button"><i className="fas fa-bars" /></a>
      </li>
    </ul>
    <ul className="navbar-nav">
      <li className="nav-item d-none d-sm-inline-block">
        <a href="#login" className="nav-link">Login/Cadastro</a>
      </li>
    </ul>
  </nav>
);
