import React from 'react';
import PropTypes from 'prop-types';

const menuTree = ({ icon, label, children }) => (
  <li className="nav-item has-treeview">
    <a href className="nav-link">
      <i className={`fa fa-${icon}`} />
      {' '}
      {label}
      <i className="right fas fa-angle-left" />
    </a>
    <ul className="nav nav-treeview">
      {children}
    </ul>
  </li>
);

menuTree.propTypes = {
  icon: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
};

export default menuTree;
