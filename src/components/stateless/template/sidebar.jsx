import React from 'react';
import Menu from './menu';

export default () => (
  <aside className="main-sidebar sidebar-dark-primary elevation-4">
    <a href="/" className="brand-link">

      <img src="assets/logo.png" alt="Charged Cloud Logo" className="brand-image img-circle elevation-3" />
      <span className="brand-text font-weight-light"> Charged Cloud</span>

    </a>
    <section className="sidebar">
      <Menu />
    </section>
  </aside>
);
