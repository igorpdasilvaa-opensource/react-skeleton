import React from 'react';
import PropTypes from 'prop-types';

function content(props) {
  const { children } = props;
  return (
    <section className="content">
      {children}
    </section>
  );
}

content.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node]).isRequired,
};

export default content;
