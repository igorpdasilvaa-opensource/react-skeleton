import React from 'react';
import MenuItem from './menuItem';
import MenuTree from './menuTree';

export default () => (
  <nav className="mt-2">
    <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <MenuItem path="#" label="Dashboard" icon="dashboard" />
      <MenuTree label="Cadastro" icon="edit">
        <MenuItem path="#billingCycles" label="Ciclos de pagamentos" icon="usd" />
      </MenuTree>
    </ul>

  </nav>
);
