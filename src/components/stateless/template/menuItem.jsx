import React from 'react';
import PropTypes from 'prop-types';

const menuItem = ({ icon, label, path }) => (
  <li className="nav-item">
    <a href={path}>
      <i className={`fa fa-${icon}`} />
      {' '}
      {label}
    </a>
  </li>
);

menuItem.propTypes = {
  icon: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
};

export default menuItem;
