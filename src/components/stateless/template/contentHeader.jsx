import React from 'react';
import PropTypes from 'prop-types';

const contentHeader = ({ title, small }) => (
  <section className="content-header">
    <h1>
      {title}
      {' '}
      <small>{small}</small>
    </h1>
  </section>
);

contentHeader.propTypes = {
  title: PropTypes.string.isRequired,
  small: PropTypes.string.isRequired,
};

export default contentHeader;
