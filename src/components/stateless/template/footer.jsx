import React from 'react';

export default () => (
  <footer className="main-footer">
    <strong>
      Copyright &copy; 2020
      <a href> Charged cloud</a>
    </strong>
  </footer>
);
