import React, { PureComponent } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import Row from '../../utils/layout/row';
import Grid from '../../utils/layout/grid';

import LabelAndInput from '../../utils/form/labelAndInput';
import LoginFields from '../stateless/loginFields';

const LoginForm = class LoginForm extends PureComponent {
  render() {
    const { handleSubmit } = this.props;
    return (
      <Row>
        <Grid cols="12 12 12 12">
          <div className="card card-primary">
            <div className="card-header">
              <h3 className="card-title">Login</h3>
            </div>
            <form onSubmit={handleSubmit}>
              <div className="card-body">
                <Row>
                  <LoginFields />
                </Row>
              </div>
              <div className="card-footer">
                <button type="submit" className="btn btn-primary">
                  Login
                </button>
              </div>
            </form>
          </div>
        </Grid>
      </Row>
    );
  }
};

// LoginForm = reduxForm({ form: 'loginForm', destroyOnUnmount: true })(LoginForm);
// const mapDispatchToProps = (dispatch) => bindActionCreators({ login }, dispatch);
// export default connect(null, mapDispatchToProps)(LoginForm);

export default reduxForm({ form: 'loginForm', destroyOnUnmount: true })(LoginForm);
