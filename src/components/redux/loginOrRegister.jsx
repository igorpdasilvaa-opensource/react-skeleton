import React, { Component } from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import If from '../../utils/operators/if';
import LoginForm from './loginForm';
import RegisterForm from './registerForm';
import { login } from '../../services/actions/auth';
import Grid from '../../utils/layout/grid';
import Row from '../../utils/layout/row';

const initialState = {
  register: false,
};

function toggle(value) {
  return !value;
}

class loginOrRegisterForm extends Component {
  state = {...initialState}

  constructor(props) {
    super(props);
    this.toggleForm = this.toggleForm.bind(this);
  }

  toggleForm() {
    this.setState((prevState) => ({ register: !prevState.register }));
    return true;
  }

  render() {
    return (
      <div>
        <If test={!this.state.register}>
          <LoginForm onSubmit={this.props.login} />
        </If>
        <If test={this.state.register}>
          <RegisterForm />
        </If>
        <Row>
          <Grid cols="12 12 12 12" >
            <button onClick={this.toggleForm} className="btn btn-secondary col-md-12">
              {this.state.register ? 'Já possui login ? Clique aqui e faça o login !' : 'Ainda não possui uma conta ? Clique aqui e registre-se !'}
            </button>
          </Grid>
        </Row>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ login }, dispatch);
export default connect(null, mapDispatchToProps)(loginOrRegisterForm);
