import React from 'react';
import Content from '../components/stateless/template/content';
import ContentHeader from '../components/stateless/template/contentHeader';

export default (props) => (
  <Content>
    <ContentHeader title="Teste" small="pequeno" />
    <p>Olá</p>
  </Content>
);
