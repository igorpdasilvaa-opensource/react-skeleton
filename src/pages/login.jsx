import React from 'react';

import LoginOrRegister from '../components/redux/loginOrRegister';
import Content from '../components/stateless/template/content';
import ContentHeader from '../components/stateless/template/contentHeader';

export default () => (
  <Content>
    <ContentHeader title="Login/Cadastro" />
    <LoginOrRegister />

  </Content>
);
